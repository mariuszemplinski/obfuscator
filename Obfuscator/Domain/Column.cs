﻿using System;
using System.Drawing;

namespace Obfuscator.Domain
{
    [Serializable]
    public class Column
    {
        public Column(Database database, Table table, string name, bool isObfuscationPossible, ColumnType type) : this(database,table, null, name, isObfuscationPossible, type)
        { }

        public Column(Database database,Table table, ColumnObfuscation obfuscation, string name, bool isObfuscationPossible, ColumnType type)
        {
            Table = table;
            Obfuscation = obfuscation;
            Name = name;
            Database = database;
            IsObfuscationPossible = isObfuscationPossible;
            Type = type;
        }

        public Table Table { get; }

        public Database Database { get; }

        public ColumnObfuscation Obfuscation { get; set; }

        public string Name { get; }
        public bool IsObfuscationPossible { get; }
        public ColumnType Type { get; }


        protected bool Equals(Column other)
        {
            return Equals(Table, other.Table) && Equals(Database, other.Database) && Name == other.Name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Column) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Table != null ? Table.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Database != null ? Database.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Column left, Column right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Column left, Column right)
        {
            return !Equals(left, right);
        }
    }

    public class ColumnType
    {
        public ColumnType(string name, string details)
        {
            Name = name;
            Details = details;
        }

        public string Name { get; }
        public string Details { get; set; }
    }
}
