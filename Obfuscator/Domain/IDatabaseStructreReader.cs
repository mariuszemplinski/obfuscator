﻿using System;
using System.Collections.Generic;

namespace Obfuscator.Domain
{
    public interface IDatabaseStructreReader
    {
        List<Column> ReaDatabase(string connectionString, Action<DatabaseOperationExecutionProgress> progessChangedHandler, Cancellation cancellation);
    }
}
