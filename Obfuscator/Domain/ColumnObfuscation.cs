﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obfuscator.Domain
{
    [Serializable]
    public class ColumnObfuscation
    {
        public ColumnObfuscation(ObfuscatorType type, List<ObfuscationParameter> parameters)
        {
            Type = type;
            Parameters = parameters;
        }

        public ObfuscatorType Type { get; }

        public List<ObfuscationParameter> Parameters { get; }

    }

    [Serializable]
    public class ObfuscationParameter
    {
        public ObfuscationParameter(ObfuscationParameter copyFrom):this(copyFrom.Key, copyFrom.Value)
        {

        }

        public ObfuscationParameter(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; }
        public string Value { get; set; }

        public bool Mandatory { get; set; }
    }


    [Serializable]
    public enum ObfuscatorType
    {
        Text,
        Numeric,
        FullName,
        Date,
        DateTime,
        Time,
        Phone,
    }
}
