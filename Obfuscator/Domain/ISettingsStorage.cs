﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obfuscator.Domain
{
    public interface ISettingsStorage
    {
        void Save(ObfuscatorServiceSettigs serviceSettigs, string fileName);
        ObfuscatorServiceSettigs Load(string fileName);
        ObfuscatorServiceSettigs CreateCopy(ObfuscatorServiceSettigs settigs);
    }
}
