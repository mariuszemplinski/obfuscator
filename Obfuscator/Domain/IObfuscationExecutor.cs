﻿using System;

namespace Obfuscator.Domain
{
    public interface IObfuscationExecutor
    {
        void Execute(Action<DatabaseOperationExecutionProgress> reportObfuscationProgress, Cancellation cancellation);
        bool Finished { get; }
    }
}