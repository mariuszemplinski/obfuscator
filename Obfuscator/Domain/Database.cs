﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo.Agent;

namespace Obfuscator.Domain
{
    [Serializable]
    public class Database
    {
        public Database(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; }

        protected bool Equals(Database other)
        {
            return ConnectionString == other.ConnectionString;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Database) obj);
        }

        public override int GetHashCode()
        {
            return (ConnectionString != null ? ConnectionString.GetHashCode() : 0);
        }

        public static bool operator ==(Database left, Database right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Database left, Database right)
        {
            return !Equals(left, right);
        }
    }
}
