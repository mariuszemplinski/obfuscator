﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;

namespace Obfuscator.Domain.Obfuscators
{
    public class PhoneObfuscator : ObfuscationHandler
    {
        Faker _phoneGenerator = new Faker("de");

        public PhoneObfuscator(List<ObfuscationParameter> parameters) : base(parameters)
        {
        }

        public override object Generate()
        {
            return _phoneGenerator.Phone.PhoneNumber();
        }
    }
}
