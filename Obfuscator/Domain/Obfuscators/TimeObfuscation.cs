﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;

namespace Obfuscator.Domain.Obfuscators
{
    public class TimeObfuscation : ObfuscationHandler
    {
        private TimeSpan _minTime;
        private TimeSpan _maxTime;

        private DateTime _minDateTime;
        private DateTime _maxDateTime;


        private const string MinTimeKey = "Min";
        private const string MaxTimeKey = "Max";

        Faker _dateGenerator = new Faker();

        public TimeObfuscation(List<ObfuscationParameter> parameters) : base(parameters)
        {
            ParseParamteres(parameters);
        }

        private void ParseParamteres(List<ObfuscationParameter> parameters)
        {
            ThrowExceptionWhenNoMandatoryParamPresent(new List<string>() { MinTimeKey, MaxTimeKey });

            var param = parameters.First(p => p.Key == MinTimeKey);

            if (!TimeSpan.TryParse(param.Value, out _minTime))
            {
                throw new ObfuscationParamException(MinTimeKey, "Can not parse dateTime", param.Value);
            }

             param = parameters.First(p => p.Key == MaxTimeKey);

            if (!TimeSpan.TryParse(param.Value, out _maxTime))
            {
                throw new ObfuscationParamException(MinTimeKey, "Can not parse dateTime", param.Value);
            }
            
            if (_maxTime < _minTime)
            {
                throw new ObfuscationParamException(null, $"{MinTimeKey} must be less than {MaxTimeKey}", null);
            }

            _minDateTime = new DateTime(2020,1,1).Add(_minTime);
            _maxDateTime = new DateTime(2020,1,1).Add(_maxTime);
        }


        public override object Generate()
        {
            return _dateGenerator.Date.Between(_minDateTime, _maxDateTime).TimeOfDay;
            
        }

        public static new List<ObfuscationParameter> ObfuscatorParameters
        {
            get
            {
                return new List<ObfuscationParameter>
                {
                    new ObfuscationParameter(MinTimeKey,null){Mandatory = true},
                    new ObfuscationParameter(MaxTimeKey,null){Mandatory = true},
                };
            }
        }
    }
}
