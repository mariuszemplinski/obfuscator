﻿using System.Collections.Generic;
using Bogus;

namespace Obfuscator.Domain
{
    public class FullNameObfuscation : ObfuscationHandler
    {
        Faker _userGeneartor = new Faker("en");

        public FullNameObfuscation(List<ObfuscationParameter> parameters) : base(parameters)
        {
        }

        public override object Generate()
        {
            return _userGeneartor.Name.FullName();
        }
    }
}