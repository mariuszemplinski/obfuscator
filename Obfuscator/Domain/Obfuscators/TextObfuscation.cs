﻿using System.Collections.Generic;
using System.Linq;
using Bogus;

namespace Obfuscator.Domain.Obfuscators
{
    public class TextObfuscation : ObfuscationHandler
    {
        Faker _textGenerator = new Faker("de");
        private int _maxLength = 0;
        private const string MaxLengthParmKey = "Max lenght";

        public TextObfuscation(List<ObfuscationParameter> parameters) : base(parameters)
        {
            ParseParameters(parameters);
        }

        private void ParseParameters(List<ObfuscationParameter> parameters)
        {
            ThrowExceptionWhenNoMandatoryParamPresent(new List<string>(){ MaxLengthParmKey });
            var param = parameters.First(p => p.Key == MaxLengthParmKey);

            if (!int.TryParse(param.Value, out _maxLength))
            {
                throw new ObfuscationParamException(MaxLengthParmKey, "Can not parse int", param.Value);
            }
        }


        public override object Generate()
        {
            return _textGenerator.Random.String2(1, _maxLength, "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }

        public new static List<ObfuscationParameter> ObfuscatorParameters
        {
            get
            {
                return new List<ObfuscationParameter>
                {
                    new ObfuscationParameter(MaxLengthParmKey, null) {Mandatory = true}
                };
            }
        }
    }
}
