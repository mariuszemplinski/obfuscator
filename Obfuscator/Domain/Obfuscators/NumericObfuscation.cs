﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;

namespace Obfuscator.Domain.Obfuscators
{
    public class NumericObfuscation : ObfuscationHandler
    {
        Faker _sumericGenerator = new Faker("de");

        private int _decimals = 0;
        private decimal? _min;
        private decimal? _max;
        
        private const string DecimalsKeyName = "Decimals";
        private const string MinValueKeyName = "Minimal value";
        private const string MaxValueKeyName = "Maximal value";

        public NumericObfuscation(List<ObfuscationParameter> parameters) : base(parameters)
        {
            ParseParameters(parameters);
        }

        private void ParseParameters(List<ObfuscationParameter> parameters)
        {
            ThrowExceptionWhenNoMandatoryParamPresent(new List<string>
                {DecimalsKeyName, MinValueKeyName, MaxValueKeyName});

            var decmalsParm = parameters.First(p => p.Key == DecimalsKeyName);
            

            if (!int.TryParse(decmalsParm.Value, out _decimals))
            {
                throw new ObfuscationParamException(DecimalsKeyName, "Can not parse int", decmalsParm.Value);
            }
            
            var minValueParm = parameters.First(p => p.Key == MinValueKeyName);
            
            if (!decimal.TryParse(minValueParm.Value, out var min))
            {
                throw new ObfuscationParamException(MinValueKeyName, "Can not parse double", decmalsParm.Value);
            }
            
            _min = min;
            
            

            var maxValueParm = parameters.First(p => p.Key == MaxValueKeyName);
            
            if (!decimal.TryParse(maxValueParm.Value, out var max))
            {
                throw new ObfuscationParamException(MaxValueKeyName, "Can not parse double", decmalsParm.Value);
            }
            else
            {
                _max = max;
            }
            

            if (_max.HasValue && !_min.HasValue || !_max.HasValue && _min.HasValue)
            {
                throw new ObfuscationParamException(null, $"Both {MinValueKeyName} and {MaxValueKeyName} must be set or any", null);
            }

            if (_max < _min)
            {
                throw new ObfuscationParamException(null, $"{MinValueKeyName} must be less than {MaxValueKeyName}", null);
            }
        }


        public override object Generate()
        {
            var resultFromMinMax = _sumericGenerator.Finance.Amount(_min.Value, _max.Value, _decimals);
            return resultFromMinMax;

        }

        public new static List<ObfuscationParameter> ObfuscatorParameters
        {
            get
            {
                return new List<ObfuscationParameter>
                {
                    new ObfuscationParameter(DecimalsKeyName, null) {Mandatory = true},
                    new ObfuscationParameter(MinValueKeyName, null) {Mandatory = true},
                    new ObfuscationParameter(MaxValueKeyName, null) {Mandatory = true}
                };
            }
        }
    }
}
