﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;

namespace Obfuscator.Domain.Obfuscators
{
    public class DateTimeObfuscation : ObfuscationHandler
    {
        private DateTime _minDate;
        private DateTime _maxDate;
        private const string MinDateKey = "Min";
        private const string MaxDateKey = "Max";
        
        Faker _dateGenerator = new Faker();

        public DateTimeObfuscation(List<ObfuscationParameter> parameters) : base(parameters)
        {
            ParseParamteres(parameters);
        }

        private void ParseParamteres(List<ObfuscationParameter> parameters)
        {
            ThrowExceptionWhenNoMandatoryParamPresent(new List<string>() { MinDateKey, MaxDateKey });

            var param = parameters.First(p => p.Key == MinDateKey);

            if (!DateTime.TryParse(param.Value, out _minDate))
            {
                throw new ObfuscationParamException(MinDateKey, "Can not parse dateTime", param.Value);
            }
            


             param = parameters.First(p => p.Key == MaxDateKey);

            if (!DateTime.TryParse(param.Value, out _maxDate))
            {
                throw new ObfuscationParamException(MinDateKey, "Can not parse dateTime", param.Value);
            }

            if (_maxDate < _minDate)
            {
                throw new ObfuscationParamException(null, $"{MinDateKey} must be less than {MaxDateKey}", null);
            }
        }


        public override object Generate()
        {
            return _dateGenerator.Date.Between(_minDate, _maxDate);
        }

        public static new List<ObfuscationParameter> ObfuscatorParameters
        {
            get
            {
                return new List<ObfuscationParameter>
                {
                    new ObfuscationParameter(MinDateKey,null){Mandatory = true},
                    new ObfuscationParameter(MaxDateKey,null){Mandatory = true},
                };
            }
        }
    }
}
