﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obfuscator.Domain.Obfuscators
{
    public class ObfuscationParamException : Exception
    {
        public ObfuscationParamException(string paramName, string message, string paramStringValue)
        {
            ParamName = paramName;
            Message = message;
            ParamStringValue = paramStringValue;
        }

        public string ParamName { get; set; }

        public string Message { get; set; }

        public string ParamStringValue { get; set; }
    }
}
