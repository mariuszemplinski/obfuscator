﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;

namespace Obfuscator.Domain.Obfuscators
{
    public class DateObfuscation : ObfuscationHandler
    {
        private DateTime _minDate;
        private DateTime _maxDate;
        private const string MinDateKey = "Min";
        private const string MaxDateKey = "Max";
        Random _random = new Random(DateTime.UtcNow.Millisecond);
        private int _dayDifference;

        public DateObfuscation(List<ObfuscationParameter> parameters) : base(parameters)
        {
            ParseParamteres(parameters);
        }

        private void ParseParamteres(List<ObfuscationParameter> parameters)
        {
            ThrowExceptionWhenNoMandatoryParamPresent(new List<string>() { MinDateKey, MaxDateKey });

            var param = parameters.First(p => p.Key == MinDateKey);

            if (!DateTime.TryParse(param.Value, out _minDate))
            {
                throw new ObfuscationParamException(MinDateKey, "Can not parse dateTime", param.Value);
            }

             param = parameters.First(p => p.Key == MaxDateKey);

            if (!DateTime.TryParse(param.Value, out _maxDate))
            {
                throw new ObfuscationParamException(MinDateKey, "Can not parse dateTime", param.Value);
            }

            _minDate = new DateTime(_minDate.Year, _minDate.Month, _minDate.Day);

            if (_maxDate < _minDate)
            {
                throw new ObfuscationParamException(null, $"{MinDateKey} must be less than {MaxDateKey}", null);
            }

            _dayDifference = (int)(_maxDate - _minDate).TotalDays;
        }


        public override object Generate()
        {
            var addDays = _random.Next(0, _dayDifference);

            return _minDate.AddDays(addDays);
        }

        public static new List<ObfuscationParameter> ObfuscatorParameters
        {
            get
            {
                return new List<ObfuscationParameter>
                {
                    new ObfuscationParameter(MinDateKey,null){Mandatory = true},
                    new ObfuscationParameter(MaxDateKey,null){Mandatory = true},
                };
            }
        }
    }
}
