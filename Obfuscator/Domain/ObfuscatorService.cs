﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Obfuscator.Domain
{
    [Serializable]
    public class ObfuscatorService
    {
        private readonly IDatabaseStructreReader _databaseStructreReader;
        private readonly IObfucationExecutorFactory _obfucationExecutorFactory;
        private ISettingsStorage _settingsStorage;
        private readonly IObfuscationHandlerFactory _obfuscationHandlerFactory;

        private ObfuscatorServiceSettigs _settings = new ObfuscatorServiceSettigs();

        public ObfuscatorService(IDatabaseStructreReader databaseStructreReader, IObfucationExecutorFactory obfucationExecutorFactory, ISettingsStorage settingsStorage, IObfuscationHandlerFactory obfuscationHandlerFactory)
        {
            _databaseStructreReader = databaseStructreReader;
            _obfucationExecutorFactory = obfucationExecutorFactory;
            _settingsStorage = settingsStorage;
            _obfuscationHandlerFactory = obfuscationHandlerFactory;
        }
        
        public void LoadSettingsFromFile(string fileName, Action<DatabaseOperationExecutionProgress> progessChangedHandler, Cancellation cancellation)
        {
            _settings.Clear();

            var settingsFromFile = _settingsStorage.Load(fileName);

            foreach (Database database in settingsFromFile.Databases)
            {
                var stopwatch = Stopwatch.StartNew();
                var columns = _databaseStructreReader.ReaDatabase(database.ConnectionString,  progessChangedHandler, cancellation);
                stopwatch.Stop();
                Console.WriteLine(stopwatch.ElapsedMilliseconds);
                foreach (Column column in columns)
                {
                    _settings.AddColumnFromDatabase(column);
                }
            }
            _settings.AddSeetingsFromFile(settingsFromFile);
        }

        public void SaveSettingsToFile(string fileName)
        {
            _settingsStorage.Save(_settings, fileName);
        }

        public void AddDatabase(string connectionString, Action<DatabaseOperationExecutionProgress> progessChangedHandler, Cancellation cancellation)
        {
            if (_settings.Databases.Any(d => d.ConnectionString == connectionString))
            {
                return;
            }

            var columns = _databaseStructreReader.ReaDatabase(connectionString, progessChangedHandler, cancellation);

            if (columns != null)
            {
                foreach (Column column in columns)
                {
                    _settings.AddColumnFromDatabase(column);
                }

                _settings.AddDatabaseIfNotExists(new Database(connectionString));
            }
        }

        public void PerformObfuscation(Database database, Action<DatabaseOperationExecutionProgress> progessChangedHandler, Cancellation cancellation)
        {
            IObfuscationExecutor executor = _obfucationExecutorFactory.CreatExecutor(database, _settings);
            executor.Execute(progessChangedHandler, cancellation);
            _obfucationExecutorFactory.Destroy(executor);
        }

        public ObfuscatorServiceSettigs GetSettings()
        {
            var settings =  _settingsStorage.CreateCopy(_settings);
            return settings;
        }

        public void RemoveDatabase(Database database)
        {
            if (database == null)
            {
                return;
            }
            _settings.RemoveDatabase(database);
        }

        public void SaveObfuscationSettings(List<Column> columnsToSetObfuscation)
        {
            _settings.SetColumnsObfuscation(columnsToSetObfuscation);
        }

        public List<ObfuscationParameter> GetParamsForExecutor(ObfuscatorType obfuscationType)
        {
            return _obfuscationHandlerFactory.GetObfuscationParameters(obfuscationType);
        }
    }

    public class Cancellation
    {
        public bool CancellRequested { get; set; }
    }
}
