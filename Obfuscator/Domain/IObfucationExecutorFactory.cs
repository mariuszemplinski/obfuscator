﻿namespace Obfuscator.Domain
{
    public interface IObfucationExecutorFactory
    {
        IObfuscationExecutor CreatExecutor(Database database, ObfuscatorServiceSettigs serviceSettigs);
        void Destroy(IObfuscationExecutor executor);
    }
}