﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus.DataSets;
using Obfuscator.Domain.Obfuscators;

namespace Obfuscator.Domain
{
    public interface IObfuscationHandlerFactory
    {
        ObfuscationHandler CreateObfuscation(Column column);
        List<ObfuscationParameter> GetObfuscationParameters(ObfuscatorType obfuscatorType);
    }

    public class ObfuscationHandlerFactory : IObfuscationHandlerFactory
    {
        public ObfuscationHandler CreateObfuscation(Column column)
        {
            if (column.Obfuscation == null)
            {
                return null;
            }

            switch (column.Obfuscation.Type)
            {
                case ObfuscatorType.FullName:
                    return new FullNameObfuscation(column.Obfuscation.Parameters);
                case ObfuscatorType.Phone:
                    return new PhoneObfuscator(column.Obfuscation.Parameters);
                case ObfuscatorType.Text:
                    return new TextObfuscation(column.Obfuscation.Parameters);
                case ObfuscatorType.Numeric:
                    return new NumericObfuscation(column.Obfuscation.Parameters);
                case ObfuscatorType.Date:
                    return new DateObfuscation(column.Obfuscation.Parameters);
                case ObfuscatorType.DateTime:
                    return new DateTimeObfuscation(column.Obfuscation.Parameters);
                case ObfuscatorType.Time:
                    return new TimeObfuscation(column.Obfuscation.Parameters);
            }

            throw new NotSupportedException(column.Obfuscation.Type.ToString());
        }

        public List<ObfuscationParameter> GetObfuscationParameters(ObfuscatorType obfuscatorType)
        {
            switch (obfuscatorType)
            {
                case ObfuscatorType.Text:
                    return TextObfuscation.ObfuscatorParameters;
                case ObfuscatorType.Numeric:
                    return NumericObfuscation.ObfuscatorParameters;
                case ObfuscatorType.Date:
                    return DateObfuscation.ObfuscatorParameters;
                case ObfuscatorType.DateTime:
                    return DateTimeObfuscation.ObfuscatorParameters;
                case ObfuscatorType.Time:
                    return TimeObfuscation.ObfuscatorParameters;
                default:
                    return new List<ObfuscationParameter>();
            }
        }
    }
}
