﻿namespace Obfuscator.Domain
{
    public class DatabaseOperationExecutionProgress
    {
        public double TotalProrogressPercent { get; set; } 
        public Table CurrentTable { get; set; }
        public double CurrentTableProgressPercent { get; set; }

        public string Message { get; set; }

        public  bool Finished { get; set; }
    }
}