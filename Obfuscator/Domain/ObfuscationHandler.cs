﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obfuscator.Domain.Obfuscators;

namespace Obfuscator.Domain
{
    public abstract class ObfuscationHandler
    {
        protected readonly List<ObfuscationParameter> _parameters;
     


        public ObfuscationHandler(List<ObfuscationParameter> parameters)
        {
            _parameters = parameters;
        }

        public abstract object Generate();

        public static List<ObfuscationParameter> ObfuscatorParameters
        {
            get
            {
                return new List<ObfuscationParameter>();
            }
        }

        protected void ThrowExceptionWhenNoMandatoryParamPresent(List<string> manadatoryParamKeys)
        {
            if (manadatoryParamKeys.Count == 0)
            {
                return;
            }

            foreach (string manadatoryParamKey in manadatoryParamKeys)
            {
                var param = _parameters.FirstOrDefault(p => p.Key == manadatoryParamKey);
                if (param is null)
                {
                    throw new ObfuscationParamException(manadatoryParamKey, "No param found", null);
                }
            }

            
        }
    }
}
