﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Obfuscator.Domain
{
    public interface IObfuscatorServiceSettigs
    {
        List<Database> Databases { get; }
        List<Column> Columns { get; }
    }

    public class ObfuscatorServiceSettigs : IObfuscatorServiceSettigs
    {
        public ObfuscatorServiceSettigs()
        {
            Databases = new List<Database>();
            Columns= new List<Column>();
        }

        public List<Database> Databases { get; }
        public List<Column> Columns { get; }

        public void AddColumnFromDatabase(Column column)
        {
            Columns.Add(column);
        }

        public void AddDatabaseIfNotExists(Database database)
        {
            if (!Databases.Contains(database))
            {
                Databases.Add(database);
            }
        }

        public void AddSeetingsFromFile(ObfuscatorServiceSettigs settingsFromFile)
        {
            foreach (Database database in settingsFromFile.Databases)
            {
                AddDatabaseIfNotExists(database);
            }
            
            foreach (Column column in settingsFromFile.Columns)
            {
                UpdateColumnFromFile(column);
            }
        }

        private void UpdateColumnFromFile(Column column)
        {
            var existingColumn = Columns.FirstOrDefault(c => c == column);
            if (existingColumn != null)
            {
                if (existingColumn.IsObfuscationPossible != column.IsObfuscationPossible || column.Obfuscation != null)
                {
                    Columns.Remove(existingColumn);
                    Column replacement  = new Column(column.Database, column.Table, column.Obfuscation, column.Name, existingColumn.IsObfuscationPossible, existingColumn.Type);
                    Columns.Add(replacement);
                }
            }
        }

        public void Clear()
        {
            Columns.Clear();
            Databases.Clear();
        }

        public void RemoveDatabase(Database database)
        {
            Databases.Remove(database);

            List<Column> columnsToRemove = new List<Column>();
            foreach (Column column in Columns)
            {
                if (column.Database == database)
                {
                    columnsToRemove.Add(column);
                }
            }

            foreach (Column column in columnsToRemove)
            {
                Columns.Remove(column);
            }
        }

        public void SetColumnsObfuscation(List<Column> columnsToSetObfuscation)
        {
            foreach (var column in columnsToSetObfuscation)
            {
                var existingColumn = Columns.FirstOrDefault(c => c == column);
                if (existingColumn != null)
                {
                    if (column.Obfuscation != null)
                    {
                        existingColumn.Obfuscation = new ColumnObfuscation(column.Obfuscation.Type,
                            column.Obfuscation.Parameters.Select(o => new ObfuscationParameter(o)).ToList());
                    }
                    else
                    {
                        existingColumn.Obfuscation = null;
                    }
                }

            }
        }
    }
}