﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Obfuscator.Domain;

namespace Obfuscator.Gui
{
    public partial class ObfuscationParametersForm : DevExpress.XtraEditors.XtraForm
    {
        public ObfuscationParametersForm()
        {
            Parameters = new BindingList<ObfuscationParameter>();

            InitializeComponent();
            gridControl1.DataSource = Parameters /* TODO: Assign the real data here.*/;
            gridView1.KeyDown += GridView1_KeyDown;
            
        }

        private void GridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                GridView view = sender as GridView;

                var param = Parameters[view.FocusedRowHandle];

                if (param.Mandatory)
                {
                    return;
                }

                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                    DialogResult.Yes)
                    return;
                
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        public void LoadParameters(List<ObfuscationParameter> parameters, string columnName, ColumnType columnType)
        {
            this.Text += " - " + columnName;
            
            this.richTextBox1.SelectionFont= new Font(this.Font.Name, 10, FontStyle.Bold);
            this.richTextBox1.AppendText("SQL type info:"+Environment.NewLine);
            this.richTextBox1.SelectionFont = new Font(this.Font.Name, 9, FontStyle.Regular);
            this.richTextBox1.AppendText("Name: "+columnType.Name + Environment.NewLine);
            this.richTextBox1.AppendText(columnType.Details.Replace("\n", Environment.NewLine));

            foreach (ObfuscationParameter obfuscationParameter in parameters)
            {
                Parameters.Add(obfuscationParameter);
            }
        }


        public BindingList<ObfuscationParameter> Parameters { get; }
    }
}