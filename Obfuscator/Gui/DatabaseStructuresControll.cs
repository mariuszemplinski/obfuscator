﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using Obfuscator.Domain;

namespace Obfuscator.Gui
{
    public partial class DatabaseStructuresControll : XtraUserControl
    {
        public DatabaseStructuresControll()
        {
            Structures = new BindingList<DatabaseStructureNode>();
            InitializeComponent();

            databaseStructureTree.ParentFieldName = "ParentId";
            databaseStructureTree.KeyFieldName = "Id";
            databaseStructureTree.DataSource = Structures;

            var obfuscationVisibleColumn = databaseStructureTree.Columns.AddVisible("IsObfuscationPossible");
            obfuscationVisibleColumn.Caption = "IsObfuscationPossible";
            obfuscationVisibleColumn.UnboundType = UnboundColumnType.Object;
            obfuscationVisibleColumn.ColumnEdit = repositoryItemPictureEdit1;
            obfuscationVisibleColumn.Visible = true;
            obfuscationVisibleColumn.OptionsColumn.ReadOnly = true;

            this.MenuItemPhone.Click += SetPhoneObfuscator;
            this.MenuItemFullName.Click += SetFullNameObfuscator;
            this.MenuItemClear.Click += SetClearObfuscation;
            this.MenuItemText.Click += SetTextObfuscation;
            this.MenuItemDate.Click += SetDateObfuscation;
            this.MenuItemDateTime.Click += SetDateTimeObfuscation;
            this.MenuItemTime.Click += SetTimeObfuscation;
        }

        private void SetTimeObfuscation(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.Time);
        }

        private void SetDateTimeObfuscation(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.DateTime);
        }

        private void SetDateObfuscation(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.Date);
        }

        private void NumericObfuscation(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.Numeric);
        }

        private void SetTextObfuscation(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.Text);
        }
        private void SetClearObfuscation(object sender, EventArgs e)
        {
            SetObfuscation(null);
        }

        private void SetFullNameObfuscator(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.FullName);
        }

        private void SetPhoneObfuscator(object sender, EventArgs e)
        {
            SetObfuscation(ObfuscatorType.Phone);
        }

        
        private void SetObfuscation(ObfuscatorType? obfuscatorType)
        {
            List<DatabaseStructureNode> nodesToSetObfuscation = databaseStructureTree.Selection.Select(node => Structures[node.Id]).ToList();

            SetObfuscationRequested(nodesToSetObfuscation, obfuscatorType);
            
            this.databaseStructureTree.Refresh();
        }

        public event Action<List<DatabaseStructureNode>, ObfuscatorType?> SetObfuscationRequested = delegate { };

        public BindingList<DatabaseStructureNode> Structures { get; }

        public void Reload(List<DatabaseStructureNode> nodes)
        {
            databaseStructureTree.BeginUpdate();
            Structures.Clear();
            foreach (DatabaseStructureNode databaseStructureNode in nodes)
            {
                Structures.Add(databaseStructureNode);
            }
            databaseStructureTree.EndUpdate();

        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddDatabaseForm addDatabaseForm = new AddDatabaseForm();
            
            var dialogResult = addDatabaseForm.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                AddDatabaseRequested(addDatabaseForm.DatabaseConnectionString);
            }
        }

        public event Action<string> AddDatabaseRequested = delegate { };

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.Multiselect = false;

            var result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    OpenFileRequested(openFileDialog.FileName);
                }
                catch(Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error while loading data", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        public event Action<string> OpenFileRequested = delegate { };

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.OverwritePrompt = true;

            var result = saveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    SaveFileRequested(saveFileDialog.FileName);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error while saving data", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        public event Action<string> SaveFileRequested = delegate { };

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var s = databaseStructureTree.Selection;
            if (s.Count == 0)
            {
                MessageBox.Show("Nothing selected. Select object from database to remove");
                return;
            }
            
            var selectedDatabaseStructureNode = Structures[s[0].Id];

            var confirmationResult =
                MessageBox.Show("Remove database that contains element " + selectedDatabaseStructureNode.DisplayText, "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (confirmationResult == DialogResult.OK)
            {
                RemoveDatabaseRequested(selectedDatabaseStructureNode);
            }
        }

        public event Action<DatabaseStructureNode> RemoveDatabaseRequested = delegate { };

        private void executeObfuscationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var s = databaseStructureTree.Selection;
            if (s.Count == 0)
            {
                MessageBox.Show("Nothing selected. Select object from database to execute obfuscation.");
                return;
            }

            var selectedDatabaseStructureNode = Structures[s[0].Id];

            var confirmationResult =
                MessageBox.Show("Execute obfuscation for database that contains element " + selectedDatabaseStructureNode.DisplayText, "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (confirmationResult == DialogResult.OK)
            {
                ExecuteObfuscationRequested(selectedDatabaseStructureNode);
            }
        }

        public event Action<DatabaseStructureNode> ExecuteObfuscationRequested = delegate { };

        private void toolTipController1_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl.Name == databaseStructureTree.Name)
            {
                DevExpress.Utils.SuperToolTip superToolTip = new DevExpress.Utils.SuperToolTip();
                DevExpress.Utils.ToolTipItem toolTipItem = new DevExpress.Utils.ToolTipItem();

                superToolTip.Items.Add(toolTipItem);
                DevExpress.Utils.ToolTipControlInfo myInfo = new DevExpress.Utils.ToolTipControlInfo
                {
                    SuperTip = superToolTip
                };
                DevExpress.XtraTreeList.TreeListHitInfo hi = databaseStructureTree.CalcHitInfo(e.ControlMousePosition);
                if (hi.Node != null && hi.Column != null)
                {

                    if (hi.Column.FieldName == typeNameColumn.FieldName)
                    {
                        var sqlTypeDetails = this.Structures[hi.Node.Id].SqlTypeDetails;
                        myInfo.Object = sqlTypeDetails;
                        toolTipItem.Text = sqlTypeDetails;
                        e.Info = myInfo;
                    }
                }
               
            }
        }
    }
}
