﻿using Obfuscator.Domain;

namespace Obfuscator.Gui
{
    public class DatabaseStructureNode
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string DisplayText { get; set; }
        
        public System.Drawing.Bitmap IsObfuscationPossible { get; set; }

        public object Tag { get; set; }

        public  ObfuscatorType? ObfucationType { get; set; }

        public string SqlTypeName { get; set; }

        public string SqlTypeDetails { get; set; }
    }
}
