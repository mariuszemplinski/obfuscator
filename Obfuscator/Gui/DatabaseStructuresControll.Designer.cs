﻿using DevExpress.XtraBars.Ribbon.Gallery;

namespace Obfuscator.Gui
{
    partial class DatabaseStructuresControll
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatabaseStructuresControll));
            this.databaseStructureTree = new DevExpress.XtraTreeList.TreeList();
            this.colDisplayText = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.isContainedInUniqueIndex = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.typeNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.obfuscationTypeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemText = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemNumeric = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemDateTime = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemDate = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemTime = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemFullName = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemPhone = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemClear = new System.Windows.Forms.ToolStripMenuItem();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.isPossibleToObfuscateImgaesList = new DevExpress.Utils.ImageCollection(this.components);
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.executeObfuscationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseStructureTree)).BeginInit();
            this.treeListContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isPossibleToObfuscateImgaesList)).BeginInit();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // databaseStructureTree
            // 
            this.databaseStructureTree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colDisplayText,
            this.isContainedInUniqueIndex,
            this.typeNameColumn,
            this.obfuscationTypeColumn});
            this.databaseStructureTree.ContextMenuStrip = this.treeListContextMenu;
            this.databaseStructureTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.databaseStructureTree.Location = new System.Drawing.Point(0, 24);
            this.databaseStructureTree.Name = "databaseStructureTree";
            this.databaseStructureTree.OptionsSelection.MultiSelect = true;
            this.databaseStructureTree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemMemoEdit1});
            this.databaseStructureTree.Size = new System.Drawing.Size(376, 232);
            this.databaseStructureTree.TabIndex = 0;
            this.databaseStructureTree.ToolTipController = this.toolTipController1;
            // 
            // colDisplayText
            // 
            this.colDisplayText.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colDisplayText.FieldName = "DisplayText";
            this.colDisplayText.Name = "colDisplayText";
            this.colDisplayText.OptionsColumn.AllowEdit = false;
            this.colDisplayText.OptionsColumn.ReadOnly = true;
            this.colDisplayText.OptionsColumn.ShowInExpressionEditor = false;
            this.colDisplayText.Visible = true;
            this.colDisplayText.VisibleIndex = 0;
            // 
            // isContainedInUniqueIndex
            // 
            this.isContainedInUniqueIndex.ColumnEdit = this.repositoryItemMemoEdit1;
            this.isContainedInUniqueIndex.Name = "isContainedInUniqueIndex";
            this.isContainedInUniqueIndex.OptionsColumn.ReadOnly = true;
            this.isContainedInUniqueIndex.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Object;
            // 
            // typeNameColumn
            // 
            this.typeNameColumn.Caption = "SQL type";
            this.typeNameColumn.ColumnEdit = this.repositoryItemMemoEdit1;
            this.typeNameColumn.FieldName = "SqlTypeName";
            this.typeNameColumn.Name = "typeNameColumn";
            this.typeNameColumn.OptionsColumn.AllowEdit = false;
            this.typeNameColumn.OptionsColumn.ReadOnly = true;
            this.typeNameColumn.Visible = true;
            this.typeNameColumn.VisibleIndex = 2;
            // 
            // obfuscationTypeColumn
            // 
            this.obfuscationTypeColumn.Caption = "Obfuscation";
            this.obfuscationTypeColumn.ColumnEdit = this.repositoryItemMemoEdit1;
            this.obfuscationTypeColumn.FieldName = "ObfucationType";
            this.obfuscationTypeColumn.Name = "obfuscationTypeColumn";
            this.obfuscationTypeColumn.OptionsColumn.AllowEdit = false;
            this.obfuscationTypeColumn.OptionsColumn.ReadOnly = true;
            this.obfuscationTypeColumn.Visible = true;
            this.obfuscationTypeColumn.VisibleIndex = 1;
            // 
            // treeListContextMenu
            // 
            this.treeListContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemText,
            this.MenuItemNumeric,
            this.MenuItemDateTime,
            this.MenuItemDate,
            this.MenuItemTime,
            this.MenuItemFullName,
            this.MenuItemPhone,
            this.toolStripSeparator1,
            this.MenuItemClear});
            this.treeListContextMenu.Name = "treeListContextMenu";
            this.treeListContextMenu.Size = new System.Drawing.Size(127, 186);
            // 
            // MenuItemText
            // 
            this.MenuItemText.Name = "MenuItemText";
            this.MenuItemText.Size = new System.Drawing.Size(126, 22);
            this.MenuItemText.Text = "Text";
            // 
            // MenuItemNumeric
            // 
            this.MenuItemNumeric.Name = "MenuItemNumeric";
            this.MenuItemNumeric.Size = new System.Drawing.Size(126, 22);
            this.MenuItemNumeric.Text = "Numeric";
            this.MenuItemNumeric.Click += new System.EventHandler(this.NumericObfuscation);
            // 
            // MenuItemDateTime
            // 
            this.MenuItemDateTime.Name = "MenuItemDateTime";
            this.MenuItemDateTime.Size = new System.Drawing.Size(126, 22);
            this.MenuItemDateTime.Text = "Datetime";
            // 
            // MenuItemDate
            // 
            this.MenuItemDate.Name = "MenuItemDate";
            this.MenuItemDate.Size = new System.Drawing.Size(126, 22);
            this.MenuItemDate.Text = "Date";
            // 
            // MenuItemTime
            // 
            this.MenuItemTime.Name = "MenuItemTime";
            this.MenuItemTime.Size = new System.Drawing.Size(126, 22);
            this.MenuItemTime.Text = "Time";
            // 
            // MenuItemFullName
            // 
            this.MenuItemFullName.Name = "MenuItemFullName";
            this.MenuItemFullName.Size = new System.Drawing.Size(126, 22);
            this.MenuItemFullName.Text = "Full name";
            // 
            // MenuItemPhone
            // 
            this.MenuItemPhone.Name = "MenuItemPhone";
            this.MenuItemPhone.Size = new System.Drawing.Size(126, 22);
            this.MenuItemPhone.Text = "Phone";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(123, 6);
            // 
            // MenuItemClear
            // 
            this.MenuItemClear.Name = "MenuItemClear";
            this.MenuItemClear.Size = new System.Drawing.Size(126, 22);
            this.MenuItemClear.Text = "Clear";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // toolTipController1
            // 
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // isPossibleToObfuscateImgaesList
            // 
            this.isPossibleToObfuscateImgaesList.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("isPossibleToObfuscateImgaesList.ImageStream")));
            this.isPossibleToObfuscateImgaesList.InsertImage(global::Obfuscator.Gui.GuiResources.GreenDot, "GreenDot", typeof(global::Obfuscator.Gui.GuiResources), 0);
            this.isPossibleToObfuscateImgaesList.Images.SetKeyName(0, "GreenDot");
            this.isPossibleToObfuscateImgaesList.InsertImage(global::Obfuscator.Gui.GuiResources.RedDot, "RedDot", typeof(global::Obfuscator.Gui.GuiResources), 1);
            this.isPossibleToObfuscateImgaesList.Images.SetKeyName(1, "RedDot");
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.databaseToolStripMenuItem,
            this.executeObfuscationToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(376, 24);
            this.menu.TabIndex = 1;
            this.menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // executeObfuscationToolStripMenuItem
            // 
            this.executeObfuscationToolStripMenuItem.Name = "executeObfuscationToolStripMenuItem";
            this.executeObfuscationToolStripMenuItem.Size = new System.Drawing.Size(126, 20);
            this.executeObfuscationToolStripMenuItem.Text = "Execute obfuscation";
            this.executeObfuscationToolStripMenuItem.Click += new System.EventHandler(this.executeObfuscationToolStripMenuItem_Click);
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // DatabaseStructuresControll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.databaseStructureTree);
            this.Controls.Add(this.menu);
            this.Name = "DatabaseStructuresControll";
            this.Size = new System.Drawing.Size(376, 256);
            ((System.ComponentModel.ISupportInitialize)(this.databaseStructureTree)).EndInit();
            this.treeListContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isPossibleToObfuscateImgaesList)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDisplayText;
        private DevExpress.XtraTreeList.Columns.TreeListColumn isContainedInUniqueIndex;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        public DevExpress.Utils.ImageCollection isPossibleToObfuscateImgaesList;
        public DevExpress.XtraTreeList.TreeList databaseStructureTree;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip treeListContextMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFullName;
        private System.Windows.Forms.ToolStripMenuItem MenuItemPhone;
        private System.Windows.Forms.ToolStripMenuItem executeObfuscationToolStripMenuItem;
        private DevExpress.XtraTreeList.Columns.TreeListColumn obfuscationTypeColumn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemClear;
        private DevExpress.XtraTreeList.Columns.TreeListColumn typeNameColumn;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemText;
        private System.Windows.Forms.ToolStripMenuItem MenuItemNumeric;
        private System.Windows.Forms.ToolStripMenuItem MenuItemDate;
        private System.Windows.Forms.ToolStripMenuItem MenuItemDateTime;
        private System.Windows.Forms.ToolStripMenuItem MenuItemTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
    }
}
