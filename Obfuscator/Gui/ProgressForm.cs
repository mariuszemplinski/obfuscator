﻿using System;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Obfuscator.Domain;

namespace Obfuscator.Gui
{
    public partial class ProgressForm : XtraForm
    {
        private Thread _operationThread;
        private Cancellation _cancellation;
        private bool _closeOnFinish;
        private bool _operationFinished = false;

        private Action<Cancellation> _operationToPerform;

        public ProgressForm()
        {
            InitializeComponent();
        }
        
        public void IntializeData(Action<Cancellation> operationToPerform, string operationName, bool closeOnFinish)
        {
            _closeOnFinish = closeOnFinish;
            _operationToPerform = operationToPerform;
            this.Name = operationName;
        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {
            if (_operationToPerform != null)
            {
                _operationThread = new Thread(ExecutionMethod);
                _operationThread.Start();
            }
        }

        private void ExecutionMethod()
        {
            _cancellation = new Cancellation();

            try
            {
                _operationToPerform(_cancellation);
                _operationFinished = true;
                if (_closeOnFinish)
                {
                    this.Invoke((MethodInvoker) (this.Close));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error while executing operation: " + e.Message, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public void ReportProgress(string message, int totalProgress, int operationProgress, string operationName)
        {
            try
            {
                Invoke((MethodInvoker)(() =>
                {
                    this.listBoxControl1.Items.Insert(0, message);
                    this.totalProgess.Position = totalProgress;
                    this.tableProgess.Position = operationProgress;
                    this.tableLabel.Text = operationName;
                }));
            }
            catch (Exception e)
            {
                
            }
            
        }

        private void ProgressForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CancelExecution(e);
        }

        private void CancelExecution(FormClosingEventArgs e)
        {
            if (_operationFinished)
            {
                return;
            }

            if (_operationThread.IsAlive && !_cancellation.CancellRequested)
            {
                if (e != null)
                {
                    e.Cancel = true;
                }
                var stop = MessageBox.Show("Operation in progress. Try to stop?", "In progress!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (stop == DialogResult.Yes)
                {
                    if (_cancellation != null)
                    {
                        _cancellation.CancellRequested = true;
                    }
                }
            }
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            CancelExecution(null);
        }
    }
}