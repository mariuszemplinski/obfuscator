﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using Obfuscator.Domain;

namespace Obfuscator.Gui
{
    public class DatabaseStructuresController
    {
        private readonly ObfuscatorService _obfuscatorService;
        private readonly DatabaseStructuresControll _databaseStructuresControll;
        private ObfuscatorServiceSettigs _curentlyLoadedSettigs;

        public DatabaseStructuresController(ObfuscatorService obfuscatorService, DatabaseStructuresControll databaseStructuresControll)
        {
            _obfuscatorService = obfuscatorService;
            _databaseStructuresControll = databaseStructuresControll;
            _databaseStructuresControll.databaseStructureTree.DoubleClick += DatabaseStructureTree_DoubleClick;
            _databaseStructuresControll.AddDatabaseRequested += OnDatabaseAddRequested;
            _databaseStructuresControll.SaveFileRequested += OnSaveFileRequested;
            _databaseStructuresControll.OpenFileRequested += OnOpenFileRequested;
            _databaseStructuresControll.RemoveDatabaseRequested += OnRemoveDatabaseRequested;
            _databaseStructuresControll.SetObfuscationRequested += OnSetObfuscationRequested;
            _databaseStructuresControll.ExecuteObfuscationRequested += OnExecuteObfuscationRequested;

            ReloadDatabaseStructure();
        }

        private void OnExecuteObfuscationRequested(DatabaseStructureNode databaseStructureNode)
        {
            Database databaseToObfuscate = null;
            if (databaseStructureNode.Tag is Database database)
            {
                databaseToObfuscate = database;
            }

            else
            {
                if (databaseStructureNode.Tag is Table table)
                {
                    var columntFromTable = _curentlyLoadedSettigs.Columns.FirstOrDefault(c => c.Table == table);
                    if (columntFromTable != null)
                    {
                        databaseToObfuscate = columntFromTable.Database;
                    }
                }
                else
                {
                    if (databaseStructureNode.Tag is Column column)
                    {
                        databaseToObfuscate = column.Database;
                    }
                }
            }

            if (databaseToObfuscate != null)
            {
                ProgressForm progressForm = new ProgressForm();

                Action<DatabaseOperationExecutionProgress> progressHandler = progress =>
                {
                    SetProgressOnProgressForm(progress, progressForm);
                };

                Action<Cancellation> obfuscationAction = cancellation =>
                {
                    _obfuscatorService.PerformObfuscation(databaseToObfuscate, progressHandler, cancellation);
                };



                progressForm.IntializeData(obfuscationAction, "Obfuscation", false);

                progressForm.ShowDialog();

            }
        }

        private void OnSetObfuscationRequested(List<DatabaseStructureNode> nodes, ObfuscatorType? obfuscationType)
        {
            if (nodes == null || nodes.Count == 0)
            {
                return;
            }

            var columns = nodes.Where(n => n.Tag is Column).Select(n => (Column) n.Tag)
                .Where(column => column.IsObfuscationPossible).ToList();

            if (columns.Count == 0)
            {
                return;
            }
            
            
            List<Column> columnsToSetObfuscation = new List<Column>();
            List<ObfuscationParameter> parmsToSet;
            if (obfuscationType.HasValue)
            {
                parmsToSet = _obfuscatorService.GetParamsForExecutor(obfuscationType.Value);
            }
            else
            {
                parmsToSet = new List<ObfuscationParameter>();
            }

            if (columns.Count == 1)
            {
                var column = columns[0];
                if (obfuscationType.HasValue && column.Obfuscation != null &&
                    column.Obfuscation.Type == obfuscationType.Value)
                {
                    parmsToSet = column.Obfuscation.Parameters;
                }
            }

            if (obfuscationType.HasValue)
            {
                ObfuscationParametersForm form = new ObfuscationParametersForm();
                form.LoadParameters(
                    parmsToSet,
                    string.Join(",", columns.Select(c => c.Name)),
                    columns.Select(c => c.Type).First());
                var dialogResult = form.ShowDialog();
                if (dialogResult != DialogResult.OK)
                {
                    return;
                }

                foreach (Column column in columns)
                {
                    column.Obfuscation = new ColumnObfuscation(obfuscationType.Value, form.Parameters.ToList());
                    columnsToSetObfuscation.Add(column);
                }
            }
            else
            {
                foreach (Column column in columns)
                {
                    column.Obfuscation = null;
                }
            }

            foreach (DatabaseStructureNode databaseStructureNode in nodes)
            {
                databaseStructureNode.ObfucationType = obfuscationType;
            }

            _obfuscatorService.SaveObfuscationSettings(columnsToSetObfuscation);
        }

        private void OnRemoveDatabaseRequested(DatabaseStructureNode databaseStructureNode)
        {
            if (databaseStructureNode.Tag is Database database)
            {
                _obfuscatorService.RemoveDatabase(database);
            }

            else
            {
                if (databaseStructureNode.Tag is Table table)
                {
                    var columntFromTable = _curentlyLoadedSettigs.Columns.FirstOrDefault(c => c.Table == table);
                    if (columntFromTable != null)
                    {
                        _obfuscatorService.RemoveDatabase(columntFromTable.Database);
                    }
                }
                else
                {
                    if (databaseStructureNode.Tag is Column column)
                    {
                        _obfuscatorService.RemoveDatabase(column.Database);
                    }
                }
            }
            ReloadDatabaseStructure();
        }

        private void OnOpenFileRequested(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                ProgressForm progressForm = new ProgressForm();

                Action<DatabaseOperationExecutionProgress> progressHandler = progress =>
                    {
                        SetProgressOnProgressForm(progress, progressForm);
                    };

                Action<Cancellation> obfuscationAction = cancellation =>
                {
                    _obfuscatorService.LoadSettingsFromFile(fileName, progressHandler, cancellation);
                };

                progressForm.IntializeData(obfuscationAction, "Reading database", true);

                progressForm.ShowDialog();
            }

            ReloadDatabaseStructure();
        }

        private void SetProgressOnProgressForm(DatabaseOperationExecutionProgress progress, ProgressForm progressForm)
        {
            progressForm.ReportProgress(
                progress.Message,
                (int)progress.TotalProrogressPercent,
                (int)progress.CurrentTableProgressPercent,
                progress.CurrentTable?.Name);
        }

        private void OnSaveFileRequested(string fileName)
        {
            _obfuscatorService.SaveSettingsToFile(fileName);
        }

        private void OnDatabaseAddRequested(string connectionString)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                ProgressForm progressForm = new ProgressForm();

                Action<DatabaseOperationExecutionProgress> progressHandler = progress =>
                {
                    SetProgressOnProgressForm(progress, progressForm);
                };

                Action<Cancellation> obfuscationAction = cancellation =>
                    {
                        _obfuscatorService.AddDatabase(connectionString, progressHandler, cancellation);
                    };

                progressForm.IntializeData(obfuscationAction, "Reading database", true);

                progressForm.ShowDialog();

            }

            ReloadDatabaseStructure();
        }

        private void DatabaseStructureTree_DoubleClick(object sender, EventArgs e)
        {
            var selection = _databaseStructuresControll.databaseStructureTree.Selection;
        }

        public void ReloadDatabaseStructure()
        {
            _curentlyLoadedSettigs = _obfuscatorService.GetSettings();

            List<DatabaseStructureNode> nodes = ConvertToNodesList(_curentlyLoadedSettigs);
            _databaseStructuresControll.Reload(nodes);

        }

        private List<DatabaseStructureNode> ConvertToNodesList(ObfuscatorServiceSettigs obfuscatorServiceSettings)
        {
            List < DatabaseStructureNode > result = new List<DatabaseStructureNode>();

            foreach (IGrouping<Database, Column> byDatabase in obfuscatorServiceSettings.Columns.GroupBy(c => c.Database))
            {
                DatabaseStructureNode databaseNode = new DatabaseStructureNode
                {
                    DisplayText = byDatabase.Key.ConnectionString,
                    Tag = byDatabase.Key,
                    Id = byDatabase.Key.ConnectionString,
                    ParentId = "",
                    IsObfuscationPossible = GuiResources.GreyDot
                };
                result.Add(databaseNode);
                

                foreach (var byTable in byDatabase.GroupBy(g => g.Table))
                {
                    DatabaseStructureNode tableNode = new DatabaseStructureNode
                    {
                        ParentId = databaseNode.Id,
                        DisplayText = byTable.Key.Name,
                        Tag = byTable.Key,
                        Id = byDatabase.Key.ConnectionString + "|" + byTable.Key.Name,
                        IsObfuscationPossible = GuiResources.GreyDot
                    };
                    
                    result.Add(tableNode);

                    foreach (Column column in byTable)
                    {
                        DatabaseStructureNode columnNode
                            = new DatabaseStructureNode
                            {
                                ParentId = tableNode.Id,
                                DisplayText = column.Name,
                                Tag = column,
                                Id = byDatabase.Key.ConnectionString + "|" + byTable.Key.Name + "|" + column.Name
                            };
                        columnNode.ObfucationType = column.Obfuscation?.Type;
                        columnNode.SqlTypeDetails = column.Type.Details;
                        columnNode.SqlTypeName = column.Type.Name;
                        GetImgeForNode(columnNode, column.IsObfuscationPossible);
                        result.Add(columnNode);
                    }
                }
            }
            
            return result;
        }

        private void GetImgeForNode(DatabaseStructureNode columnNode, bool isObfuscationPossible)
        {
            columnNode.IsObfuscationPossible = isObfuscationPossible ? GuiResources.GreenDot : GuiResources.RedDot;
        }
    }
}
