﻿namespace Obfuscator.Gui
{
    partial class ObfuscationParametersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btOk = new System.Windows.Forms.Button();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnParamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnParamValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnParamMandatory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // btOk
            // 
            this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOk.Location = new System.Drawing.Point(693, 281);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(75, 23);
            this.btOk.TabIndex = 1;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = typeof(Obfuscator.Domain.ObfuscationParameter);
            this.gridControl1.Location = new System.Drawing.Point(13, 13);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(755, 200);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnParamName,
            this.gridColumnParamValue,
            this.gridColumnParamMandatory});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // gridColumnParamName
            // 
            this.gridColumnParamName.Caption = "Name";
            this.gridColumnParamName.FieldName = "Key";
            this.gridColumnParamName.Name = "gridColumnParamName";
            this.gridColumnParamName.OptionsColumn.AllowEdit = false;
            this.gridColumnParamName.OptionsColumn.ReadOnly = true;
            this.gridColumnParamName.Visible = true;
            this.gridColumnParamName.VisibleIndex = 0;
            // 
            // gridColumnParamValue
            // 
            this.gridColumnParamValue.Caption = "Value";
            this.gridColumnParamValue.FieldName = "Value";
            this.gridColumnParamValue.Name = "gridColumnParamValue";
            this.gridColumnParamValue.Visible = true;
            this.gridColumnParamValue.VisibleIndex = 1;
            // 
            // gridColumnParamMandatory
            // 
            this.gridColumnParamMandatory.Caption = "Mandatory";
            this.gridColumnParamMandatory.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnParamMandatory.FieldName = "Mandatory";
            this.gridColumnParamMandatory.MaxWidth = 60;
            this.gridColumnParamMandatory.MinWidth = 60;
            this.gridColumnParamMandatory.Name = "gridColumnParamMandatory";
            this.gridColumnParamMandatory.OptionsColumn.AllowEdit = false;
            this.gridColumnParamMandatory.OptionsColumn.ReadOnly = true;
            this.gridColumnParamMandatory.Visible = true;
            this.gridColumnParamMandatory.VisibleIndex = 2;
            this.gridColumnParamMandatory.Width = 60;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.AcceptsTab = true;
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Location = new System.Drawing.Point(13, 220);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(674, 84);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // ObfuscationParametersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 316);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.btOk);
            this.Name = "ObfuscationParametersForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Obfuscation parametres";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btOk;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnParamName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnParamValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnParamMandatory;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}