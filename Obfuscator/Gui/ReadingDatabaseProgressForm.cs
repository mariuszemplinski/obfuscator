﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Obfuscator.Domain;

namespace Obfuscator.Gui
{
    public partial class ReadingDatabaseProgressForm : DevExpress.XtraEditors.XtraForm
    {
        private ObfuscatorService _obfuscatorService;
        private string _database;
        private Thread _thread;

        public ReadingDatabaseProgressForm()
        {
            InitializeComponent();
        }

        public void Initialize(ObfuscatorService obfuscatorService, string database)
        {
            _obfuscatorService = obfuscatorService;
            _database = database;
        }

        private void ReadingDatabaseProgressForm_Load(object sender, EventArgs e)
        {
            if (_obfuscatorService != null && !string.IsNullOrEmpty(_database))
            {
                _thread = new Thread(ThreadMethod);
                _thread.Start();
            }
        }


        private void ThreadMethod()
        {
            _obfuscatorService.AddDatabase();
        }

    }
}