﻿namespace Obfuscator.Gui
{
    public partial class AddDatabaseForm : DevExpress.XtraEditors.XtraForm
    {
        public AddDatabaseForm()
        {
            InitializeComponent();
        }

        public string DatabaseConnectionString
        {
            get { return this.textBox1.Text; }
        }
    }
}