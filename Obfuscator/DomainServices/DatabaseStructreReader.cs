﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DevExpress.Utils.Extensions;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Obfuscator.Domain;
using Column = Obfuscator.Domain.Column;
using Database = Obfuscator.Domain.Database;
using Table = Obfuscator.Domain.Table;

namespace Obfuscator.Mssql
{
    public class DatabaseStructreReader : IDatabaseStructreReader
    {
        private static readonly List<SqlDataType> supportedDataTypes= new List<SqlDataType>
        {
            SqlDataType.Numeric,
            SqlDataType.Text,
            SqlDataType.Decimal,
            SqlDataType.BigInt,
            SqlDataType.Bit,
            SqlDataType.Char,
            SqlDataType.Date,
            SqlDataType.DateTime,
            SqlDataType.DateTime2,
            SqlDataType.Float,
            SqlDataType.Int,
            SqlDataType.Money,
            SqlDataType.VarChar,
            SqlDataType.Char,
            SqlDataType.NChar,
            SqlDataType.NVarChar,
            SqlDataType.NVarCharMax,
            SqlDataType.Real,
            SqlDataType.SmallDateTime,
            SqlDataType.TinyInt,
            SqlDataType.Time,
        };


        public List<Column> ReaDatabase(string connectionString, Action<DatabaseOperationExecutionProgress> progessChangedHandler, Cancellation cancellation)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    var connection = new ServerConnection(sqlConnection);
                    Server server = new Server(connection);
                    server.SetDefaultInitFields(typeof(Microsoft.SqlServer.Management.Smo.Column), true);
                    server.SetDefaultInitFields(typeof(Microsoft.SqlServer.Management.Smo.Index), true);
                    server.SetDefaultInitFields(typeof(Microsoft.SqlServer.Management.Smo.ForeignKey), true);
                    

                    List<Column> domainColumns = new List<Column>();

                    var sqlDatabase = server.Databases[connection.CurrentDatabase];


                    Database database = new Database(connectionString);

                    var tableCount = sqlDatabase.Tables.Count;
                    int currentTable = 0;

                    foreach (Microsoft.SqlServer.Management.Smo.Table sqlTable in sqlDatabase.Tables)
                    {
                        currentTable++;
                        if (cancellation.CancellRequested)
                        {
                            return null;
                        }
                        
                        Table table = new Table(sqlTable.Name);

                        progessChangedHandler(new DatabaseOperationExecutionProgress
                        {
                            Message = $"Reading columns in table {sqlTable.Name} {currentTable}/{tableCount}",
                            TotalProrogressPercent = 100 * currentTable / tableCount,
                            CurrentTableProgressPercent = 0,
                            CurrentTable = table,
                            Finished = false
                        });

                        IndexCollection sqlTableIndexes = sqlTable.Indexes;
                        ForeignKeyCollection sqlTableForeignKeys = sqlTable.ForeignKeys;

                        // enumerate these two collections makes all properties are filled
                        // and speeds up checking if column belongs to index
                        foreach (Index sqlTableIndex in sqlTableIndexes)
                        {
                            foreach (IndexedColumn indexedColumn in sqlTableIndex.IndexedColumns)
                            {
                            }
                        }
                        
                        foreach (ForeignKey foreignKey in sqlTableForeignKeys)
                        {
                            foreach (ForeignKeyColumn foreignKeyColumn in foreignKey.Columns)
                            {
                            }
                        }
                        //--------------------------------------
                        
                        foreach (Microsoft.SqlServer.Management.Smo.Column sqlColumn in sqlTable.Columns)
                        {
                            
                            var isObfusationPossible = IsObfusationPossible(sqlTableIndexes, sqlColumn, sqlTableForeignKeys);
                            if (!isObfusationPossible)
                            {
                                continue;
                            }

                            ColumnType columnType = CreateColumnType(sqlColumn.DataType);

                            Column column = new Column(database, table, sqlColumn.Name, isObfusationPossible, columnType);
                            domainColumns.Add(column);
                        }
                    }

                    return domainColumns;
                }
                catch (Exception exception)
                {
                    throw;
                }
            }

            return null;
        }


        private ColumnType CreateColumnType(DataType sqlDataType)
        {
            //sqlDataType.SqlDataType
            string name = sqlDataType.Name;
            string details =
                $"MaximumLength: {sqlDataType.MaximumLength}\nNumericPrecision: {sqlDataType.NumericPrecision}\nNumericScale: {sqlDataType.NumericScale}";
                
            return new ColumnType(name, details);
        }

        public static bool IsObfusationPossible(IndexCollection sqlTableIndexes, Microsoft.SqlServer.Management.Smo.Column sqlColumn, ForeignKeyCollection foreignKeys)
        {
            if (!supportedDataTypes.Contains(sqlColumn.DataType.SqlDataType))
            {
                return false;
            }

            bool isObfusationPossible = true;

            foreach (Index tableIndex in sqlTableIndexes)
            {
                if (tableIndex.IsUnique && tableIndex.IndexedColumns[sqlColumn.Name] != null)
                {
                    isObfusationPossible = false;
                    break;
                }
            }

            if (isObfusationPossible)
            {
                foreach (ForeignKey foreignKey in foreignKeys)
                {
                    if (foreignKey.Columns[sqlColumn.Name] != null)
                    {
                        isObfusationPossible = false;
                        break;
                    }
                }
            }

            return isObfusationPossible;
        }
    }
}
