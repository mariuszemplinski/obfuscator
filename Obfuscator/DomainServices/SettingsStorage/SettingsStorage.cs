﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Obfuscator.Domain;

namespace Obfuscator.DomainServices.SettingsStorage
{
    public class SettingsStorage : ISettingsStorage
    {
        private readonly IObfuscationHandlerFactory _obfuscationHandlerFactory;
        private static readonly XmlSerializer SettingsSerializer = new XmlSerializer(typeof(SettingsDto));

        public SettingsStorage(IObfuscationHandlerFactory obfuscationHandlerFactory)
        {
            _obfuscationHandlerFactory = obfuscationHandlerFactory;
        }

        public void Save(ObfuscatorServiceSettigs serviceSettigs, string fileName)
        {
            var settingsDto = SettingsDto.FromObfuscatorSettigs(serviceSettigs, true);

            using (var fileStream = File.Open(fileName, FileMode.Create))
            {
                SettingsSerializer.Serialize(fileStream, settingsDto);
            }
        }

        public ObfuscatorServiceSettigs Load(string fileName)
        {
            using (var fileStream = File.Open(fileName, FileMode.Open))
            {
                var settingsDto = (SettingsDto) SettingsSerializer.Deserialize(fileStream);

                return ConvertToObfuscatorSettigs(settingsDto);
            }
        }

        public ObfuscatorServiceSettigs CreateCopy(ObfuscatorServiceSettigs settigs)
        {
            var settingsDto = SettingsDto.FromObfuscatorSettigs(settigs, false);
            using (MemoryStream ms = new MemoryStream())
            {
                SettingsSerializer.Serialize(ms, settingsDto);
                ms.Seek(0, SeekOrigin.Begin);
                settingsDto = (SettingsDto) SettingsSerializer.Deserialize(ms);
                var result = ConvertToObfuscatorSettigs(settingsDto);


                List<Column> columnsToRemove = new List<Column>();
                List<Column> columnsToAdd = new List<Column>();

                foreach (Column orginalColumn in settigs.Columns)
                {
                    var columnCopy = result.Columns.First(c => c == orginalColumn);
                    
                    columnsToRemove.Add(columnCopy);
                    var  columnBetterCopy = new Column(columnCopy.Database, columnCopy.Table, columnCopy.Obfuscation, columnCopy.Name, orginalColumn.IsObfuscationPossible, orginalColumn.Type);
                    columnsToAdd.Add(columnBetterCopy);
                    
                }

                foreach (Column toRemove in columnsToRemove)
                {
                    result.Columns.Remove(toRemove);
                }

                result.Columns.AddRange(columnsToAdd);

                return result;
            }
        }

        private ObfuscatorServiceSettigs ConvertToObfuscatorSettigs(SettingsDto settingsDto)
        {
            ObfuscatorServiceSettigs obfuscatorServiceSettigs = new ObfuscatorServiceSettigs();

            obfuscatorServiceSettigs.Databases.AddRange(settingsDto.ConnectionStrings.Select(c => new Database(c))
                .ToList());
            obfuscatorServiceSettigs.Columns.AddRange(settingsDto.Columns.Select(ConvertToColumn).ToList());

            
            RestoreParamManadatoryValue(obfuscatorServiceSettigs.Columns);

            return obfuscatorServiceSettigs;
        }

        private void RestoreParamManadatoryValue(List<Column> columns)
        {
            var groupCoulnsByObfuscatioType =
                columns.Where(c => c.Obfuscation != null).GroupBy(c => c.Obfuscation.Type);

            foreach (IGrouping<ObfuscatorType, Column> grouping in groupCoulnsByObfuscatioType)
            {
                var cleanParams = _obfuscationHandlerFactory.GetObfuscationParameters(grouping.Key);

                foreach (Column column in grouping)
                {
                    column.Obfuscation.Parameters.Join(cleanParams, c => c.Key, y => y.Key, (fromFile, cleanParam) =>
                        {
                            fromFile.Mandatory = cleanParam.Mandatory;
                            return fromFile;
                        }).ToList();
                }
            }
        }

        private Column ConvertToColumn(ColumnObfuscationDto columnObfuscationDto)
        {
            ColumnObfuscation obfuscation = null;
            if (columnObfuscationDto.Type.HasValue)
            {
                obfuscation = new ColumnObfuscation(
                    columnObfuscationDto.Type.Value,
                    columnObfuscationDto.Parameters.Select(p => new ObfuscationParameter(p.Key, p.Value)).ToList());
            }

            Column column = new Column(
                new Database(columnObfuscationDto.ConnectionString),
                new Table(columnObfuscationDto.TableName),
                obfuscation,
                columnObfuscationDto.ColumnName,
                obfuscation != null,
                null);

            return column;
        }
    }
}

