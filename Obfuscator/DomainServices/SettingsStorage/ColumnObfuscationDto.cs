﻿using System;
using System.Collections.Generic;
using Obfuscator.Domain;

namespace Obfuscator.DomainServices.SettingsStorage
{
    [Serializable]
    public class ColumnObfuscationDto
    {
        public string ConnectionString { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public ObfuscatorType? Type { get; set; }

        public List<ColumnObfuscationParametersDto> Parameters { get; set; }

        public static ColumnObfuscationDto FromColumn(Column column)
        {
            ColumnObfuscationDto columnObfuscationDto = new ColumnObfuscationDto
            {
                ColumnName = column.Name,
                ConnectionString = column.Database.ConnectionString,
                TableName = column.Table.Name,
                Parameters = new List<ColumnObfuscationParametersDto>()
            };
            if (column.Obfuscation != null)
            {
                columnObfuscationDto.Type = column.Obfuscation.Type;

                foreach (ObfuscationParameter obfuscationParameter in column.Obfuscation.Parameters)
                {
                    columnObfuscationDto.Parameters.Add(
                        ColumnObfuscationParametersDto.FromObfusicationParameter(obfuscationParameter));
                }
            }

            return columnObfuscationDto;
        }
    }
}