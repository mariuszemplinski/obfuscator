﻿using System;
using Obfuscator.Domain;

namespace Obfuscator.DomainServices.SettingsStorage
{
    [Serializable]
    public class ColumnObfuscationParametersDto
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public static ColumnObfuscationParametersDto FromObfusicationParameter(
            ObfuscationParameter obfuscationParameter)
        {
            return new ColumnObfuscationParametersDto
            {
                Key = obfuscationParameter.Key,
                Value = obfuscationParameter.Value
            };
        }
    }
}