﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obfuscator.Domain;

namespace Obfuscator.DomainServices.SettingsStorage
{
    [Serializable]
    public class SettingsDto
    {
        public List<string> ConnectionStrings { get; set; }

        public List<ColumnObfuscationDto> Columns { get; set; }

        public static SettingsDto FromObfuscatorSettigs(ObfuscatorServiceSettigs serviceSettigs, bool onlyWithObfuscation)
        {
            SettingsDto settingsDto = new SettingsDto
            {
                ConnectionStrings = serviceSettigs.Databases.Select(d => d.ConnectionString).ToList(),
                Columns = new List<ColumnObfuscationDto>()
            };

            foreach (Column column in serviceSettigs.Columns.Where(c => !onlyWithObfuscation || c.Obfuscation != null))
            {
                settingsDto.Columns.Add(ColumnObfuscationDto.FromColumn(column));
            }

            return settingsDto;
        }
    }
}