﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Obfuscator.Domain;
using Obfuscator.DomainServices.ObfuscationExecutor;
using Obfuscator.Mssql;

namespace Obfuscator.DomainServices
{
    public class ObfuscatonInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ObfuscatorService>());
            container.Register(Component.For<IDatabaseStructreReader>().ImplementedBy<DatabaseStructreReader>());
            container.Register(Component.For<IObfucationExecutorFactory>().ImplementedBy<ObfucationExecutorFactory>());
            container.Register(Component.For<ISettingsStorage>().ImplementedBy<SettingsStorage.SettingsStorage>());
            container.Register(Component.For<IObfuscationHandlerFactory>().ImplementedBy<ObfuscationHandlerFactory>());
        }
    }
}
