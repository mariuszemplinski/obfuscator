﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Obfuscator.Domain;
using Obfuscator.Mssql;
using Column = Obfuscator.Domain.Column;
using Database = Obfuscator.Domain.Database;
using Table = Obfuscator.Domain.Table;

namespace Obfuscator.DomainServices.ObfuscationExecutor
{
    public class ObfuscationExecutor : IDisposable, IObfuscationExecutor
    {
        private readonly List<Column> _columns;
        private Dictionary<Table, TableInfo> _tablesInfos = new Dictionary<Table, TableInfo>();
        private readonly string _connectionString;
        private SqlConnection _sqlConnection;
        private IObfuscationHandlerFactory _obfuscationHandlerFactory;
        private int RecordsToUpdate = 1000;
        private bool _finished = false;
        private Cancellation _cancellation;



        public ObfuscationExecutor(List<Column> columns, IObfuscationHandlerFactory obfuscationHandlerFactory)
        {
            _obfuscationHandlerFactory = obfuscationHandlerFactory;
            _columns = columns;
            _connectionString = _columns[0].Database.ConnectionString;
        }
        
        public bool Finished
        {
            get { return _finished; }
        }

        public void Execute(Action<DatabaseOperationExecutionProgress> reportObfuscationProgress, Cancellation cancellation)
        {
            _cancellation = cancellation;
            _progessChangedHandler = reportObfuscationProgress;
            try
            {
                _sqlConnection = CreateConnection();
                ReadInfosAboutTables();
                if (_cancellation.CancellRequested)
                {
                    return;
                }

                ExecuteUdate();
            }
            catch (Exception e)
            {
                this._progessChangedHandler?.Invoke(new DatabaseOperationExecutionProgress
                    {Message = e.ToString()});
            }
            finally
            {
                _finished = true;
                if (_cancellation.CancellRequested)
                {
                    this._progessChangedHandler?.Invoke(new DatabaseOperationExecutionProgress { Message = "Cancelled", TotalProrogressPercent = 100, Finished = true });
                }
                else
                {
                    this._progessChangedHandler?.Invoke(new DatabaseOperationExecutionProgress { Message = "Finished", TotalProrogressPercent = 100, Finished = true });
                }
                
            }
            
        }
        
        private string CreateSelectCommand(KeyValuePair<Table, TableInfo> keyValuePair)
        {
            string template = "SELECT {0} FROM [{1}] ORDER BY {2} OFFSET $$OFSET$$$ ROWS FETCH NEXT $$$NEXT$$$ ROWS ONLY";

            var columnsToObfuscate = keyValuePair.Value.Columns.Where(c => c.ObfuscationHandler != null).ToList();
            var indexColumns = keyValuePair.Value.IndexedColumns;
            string solumnsToSelect = string.Join(",", columnsToObfuscate.Select(c => $"[{c.Column.Name}]").Union(indexColumns.Select(i => $"[{i}]")));
            string indexColumnsNames = string.Join(",", indexColumns.Select(i => $"[{i}]"));

            string selectCommand = string.Format(template, solumnsToSelect, keyValuePair.Key.Name, indexColumnsNames);

            selectCommand = selectCommand.Replace("$$OFSET$$$", "{0}");
            selectCommand = selectCommand.Replace("$$$NEXT$$$", "{1}");

            return selectCommand;
        }

        private void ExecuteUdate()
        {
            double totalRows = _tablesInfos.Sum(i => i.Value.RecordsCount);
            double alreadyObfuscatedRows = 0;
            foreach (KeyValuePair<Table, TableInfo> keyValuePair in _tablesInfos)
            {
                if (_cancellation.CancellRequested)
                {
                    break;
                }
                if (keyValuePair.Value.Columns.All(c => c.ObfuscationHandler == null))
                {
                    continue;
                }
                
                var connectionForUpdate = CreateConnection();
                connectionForUpdate.Open();

                var command = _sqlConnection.CreateCommand();
                
                string selectCommandText = CreateSelectCommand(keyValuePair);
                
                int rowCountPerSelect = 5000;

                UpdateAndSaveUsingAdo(command, selectCommandText, rowCountPerSelect, keyValuePair, totalRows, alreadyObfuscatedRows);
                alreadyObfuscatedRows += keyValuePair.Value.RecordsCount;
            }
        }

        private void UpdateAndSaveUsingAdo(SqlCommand command, string selectCommandText,
            int rowCountPerSelect, KeyValuePair<Table, TableInfo> keyValuePair, double totalRows, double alreadyObfuscatedRows)
        {
            int ofset = 0;
            bool continueSelecting = true;

            double recordsRead = 0;

            while (continueSelecting)
            {
                if (_cancellation.CancellRequested)
                {
                    break;
                }
                command.CommandText = string.Format(selectCommandText, ofset, rowCountPerSelect);

                ReportWritingRecordsTable(keyValuePair.Key,
                    recordsRead / keyValuePair.Value.RecordsCount, totalRows,
                    alreadyObfuscatedRows + recordsRead);

                using (DataSet dataSet = new DataSet())
                {
                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                    {
                        dataAdapter.Fill(dataSet);

                        using (SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter))
                        {
                            dataAdapter.UpdateCommand = commandBuilder.GetUpdateCommand();

                            DataTable dataToObfuscate = dataSet.Tables[0];


                            recordsRead += dataToObfuscate.Rows.Count;

                            if (dataToObfuscate.Rows.Count == 0)
                            {
                                continueSelecting = false;
                            }
                            else
                            {
                                foreach (ColumnInfo columnInfo in keyValuePair.Value.Columns.Where(c =>
                                    !c.IsPartOfUniqueIndex && c.ObfuscationHandler != null))
                                {
                                    foreach (DataRow dataRow in dataToObfuscate.Rows)
                                    {
                                        var newValue = columnInfo.ObfuscationHandler.Generate();
                                        dataRow[columnInfo.Column.Name] = newValue;
                                    }
                                }

                                try
                                {
                                    dataAdapter.Update(dataSet);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                    throw;
                                }

                                ofset += rowCountPerSelect;
                            }
                        }
                    }
                }
            }
        }

        private void ReportWritingRecordsTable(Table table, double tableRatio, double totalRowsOfAllTables, double alreadyObfuscatedRowsOfAllTables)
        {
            this._progessChangedHandler?.Invoke(new DatabaseOperationExecutionProgress
            {
                Message = $"Obfuscating table {table.Name}: {100*tableRatio}%  {alreadyObfuscatedRowsOfAllTables}/{totalRowsOfAllTables} of all rows",
                TotalProrogressPercent = 5 + 95*alreadyObfuscatedRowsOfAllTables/totalRowsOfAllTables,
                CurrentTable = table,
                CurrentTableProgressPercent = 100*tableRatio
            });
        }
        

        private SqlConnection CreateConnection()
        {
            return new SqlConnection(_connectionString);
        }

        private void ReadInfosAboutTables()
        {
            var connection = new ServerConnection(_sqlConnection);
            Server server = new Server(connection);
            server.Refresh();

            var sqlDatabase = server.Databases[connection.CurrentDatabase];

            var groupByTabels = _columns.GroupBy(c => c.Table).ToList();

            int tablesCount = groupByTabels.Count;
            int currentTable = 0;

            foreach (var gropyByTable in groupByTabels)
            {
                if (_cancellation.CancellRequested)
                {
                    break;
                }
                currentTable++;
                ReportReadingInfoAboutTable(gropyByTable.Key,(double)currentTable/tablesCount);
                if (gropyByTable.All(c => c.Obfuscation == null))
                {
                    continue;
                }

                var sqlTable = sqlDatabase.Tables[gropyByTable.Key.Name];
                
                if (sqlTable != null)
                {
                    IndexCollection sqlTableIndexes = sqlTable.Indexes;
                    ForeignKeyCollection sqlTableForeignKeys = sqlTable.ForeignKeys;


                    //reading collections to cache data - to increase performace
                    foreach (Index sqlTableIndex in sqlTableIndexes)
                    {
                        foreach (IndexedColumn indexedColumn in sqlTableIndex.IndexedColumns)
                        {
                        }
                    }

                    foreach (ForeignKey foreignKey in sqlTableForeignKeys)
                    {
                        foreach (ForeignKeyColumn foreignKeyColumn in foreignKey.Columns)
                        {
                        }
                    }
                    //--------------------

                    var tableInfo = new TableInfo
                    {
                        Index = FindUniqueIndexForTable(sqlTable),
                        RecordsCount = sqlTable.RowCountAsDouble,
                        Columns = gropyByTable.Where(c => c.Obfuscation != null).Select(c => ReadColumnInfo(c, sqlTableIndexes,  sqlTableForeignKeys, sqlTable.Columns[c.Name])).ToList(),
                        IndexedColumns = new List<string>()
                    };
                    foreach (IndexedColumn indexIndexedColumn in tableInfo.Index.IndexedColumns)
                    {
                        tableInfo.IndexedColumns.Add(indexIndexedColumn.Name);
                    }
                    
                    _tablesInfos.Add(gropyByTable.Key, tableInfo);
                }
                else
                {
                    _tablesInfos.Add(gropyByTable.Key, null);
                }
            }
        }

        private ColumnInfo ReadColumnInfo(Column column, IndexCollection indexCollection,
            ForeignKeyCollection foreignKeyCollection, Microsoft.SqlServer.Management.Smo.Column sqlColumn)
        {
            ColumnInfo result = new ColumnInfo
            {
                Column = column,
                SqlColumn = sqlColumn
            };
            
            result.IsPartOfUniqueIndex = !DatabaseStructreReader.IsObfusationPossible(indexCollection, result.SqlColumn, foreignKeyCollection);
            
            if (!result.IsPartOfUniqueIndex)
            {
                result.ObfuscationHandler = _obfuscationHandlerFactory.CreateObfuscation(column);
            }

            return result;
        }

        private Index FindUniqueIndexForTable(Microsoft.SqlServer.Management.Smo.Table sqlTable)
        {
            foreach (Index tableIndex in sqlTable.Indexes)
            {
                if (tableIndex.IsClustered)
                {
                    return tableIndex;
                }
            }

            List<Index> uniqueIndexes = new List<Index>();

            foreach (Index tableIndex in sqlTable.Indexes)
            {
                if (tableIndex.IsUnique)
                {
                    uniqueIndexes.Add(tableIndex);
                }
            }

            if (uniqueIndexes.Count > 0)
            {
                return uniqueIndexes.OrderBy(i => i.IndexedColumns.Count).First();
            }

            return null;
        }


        private Action<DatabaseOperationExecutionProgress> _progessChangedHandler;
        
        public void Dispose()
        {
            if (_sqlConnection != null)
            {
                try
                {
                    _sqlConnection.Dispose();
                }
                catch (Exception e)
                {
                }
            }
        }

        private void ReportReadingInfoAboutTable(Table table, double tableRatio)
        {
            this._progessChangedHandler?.Invoke(new DatabaseOperationExecutionProgress
            {
                CurrentTable = table, CurrentTableProgressPercent = 0, TotalProrogressPercent = 5 * tableRatio,
                Message = $"Start reading info about table {table.Name}"
            });
        }
    }

    public class TableInfo
    {
        public double RecordsCount { get; set; }
        public Index Index { get; set; }
        public List<ColumnInfo> Columns { get; set; }
        public List<string> IndexedColumns { get; set; }
    }

    public class ColumnInfo
    {
        public Column Column { get; set; }
        public Microsoft.SqlServer.Management.Smo.Column SqlColumn { get; set; }
        public bool IsPartOfUniqueIndex { get; set; }
        public ObfuscationHandler ObfuscationHandler { get; set; }

    }
}
