﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obfuscator.Domain;

namespace Obfuscator.DomainServices.ObfuscationExecutor
{
    public class ObfucationExecutorFactory : IObfucationExecutorFactory
    {
        private IObfuscationHandlerFactory _obfuscationFactory;

        public ObfucationExecutorFactory(IObfuscationHandlerFactory obfuscationFactory)
        {
            _obfuscationFactory = obfuscationFactory;
        }

        public IObfuscationExecutor CreatExecutor(Database database, ObfuscatorServiceSettigs serviceSettigs)
        {
            return new ObfuscationExecutor(serviceSettigs.Columns.Where(c => c.Database == database).ToList(), _obfuscationFactory);
        }

        public void Destroy(IObfuscationExecutor executor)
        {
            if (executor == null)
            {
                return;
            }

            ((IDisposable) executor).Dispose();
        }
    }
}
