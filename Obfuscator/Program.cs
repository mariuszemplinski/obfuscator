﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Windsor;
using Obfuscator.DomainServices;

namespace Obfuscator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var container = CreateContainer();
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(container));
        }

        private static IWindsorContainer CreateContainer()
        {
            WindsorContainer container = new WindsorContainer();

            container.Install(new ObfuscatonInstaller());

            return container;
        }
    }
}
