﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Windsor;
using Obfuscator.Domain;
using Obfuscator.Gui;

namespace Obfuscator
{
    public partial class MainForm : Form
    {
        private readonly IWindsorContainer _container;


        public MainForm(IWindsorContainer container)
        {
            InitializeComponent();

            _container = container;

            var service = _container.Resolve<ObfuscatorService>();
            //service.AddDatabase(@"Data Source=localhost\SQLEXPRESS;Initial Catalog=eeipFzgDatabase;Persist Security Info=True;MultipleActiveResultSets=True;Integrated Security=SSPI");

            DatabaseStructuresController controller = new DatabaseStructuresController(service, this.databaseStructures1);

        }
    }
}
