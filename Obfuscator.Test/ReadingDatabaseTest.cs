﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using NUnit.Framework;
using Obfuscator.Domain;
using Obfuscator.Domain.Obfuscators;
using Obfuscator.DomainServices.ObfuscationExecutor;
using Obfuscator.Mssql;

namespace Obfuscator.Test
{
    [TestFixture]
    public class ReadingDatabaseTest
    {
        //[Test]
        //public void ReadDatabase()
        //{
        //    //string connectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=Z:\git\obfuscator\Obfuscator.Test\SampeDatabase.mdf;Integrated Security=True";
        //    string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=eeipFzgDatabase;Persist Security Info=True;MultipleActiveResultSets=True;Integrated Security=SSPI";
            
        //    //var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        //    //connectionString = string.Format(connectionString, dir + "\\");
        //    DatabaseStructreReader da = new DatabaseStructreReader();
        //    var columns = da.ReaDatabase(connectionString);

        //    //Assert.AreEqual(14, columns.Count);

        //    //Assert.AreEqual(2, columns.GroupBy(c => c.Table).Count());

        //    //var columnToOb = columns.FirstOrDefault(c => c.Name == "Name" && c.Table.Name == "Table");
        //    //columnToOb.Obfuscation = new ColumnObfuscation(ObfuscatorType.FullName, new List<ObfuscationParameter>());
        //    //columnToOb = columns.FirstOrDefault(c => c.Name == "Telepnone" && c.Table.Name == "Table");
        //    //columnToOb.Obfuscation = new ColumnObfuscation(ObfuscatorType.Phone, new List<ObfuscationParameter>());

        //    var columnToOb = columns.FirstOrDefault(c => c.Name == "GeaendertDurch" && c.Table.Name == "Meilenstein");
        //    columnToOb.Obfuscation = new ColumnObfuscation(ObfuscatorType.FullName, new List<ObfuscationParameter>());

        //    ObfuscationExecutor obfuscationExecutor = new ObfuscationExecutor(columns, null);
        //    obfuscationExecutor.Execute(null, new Cancellation());
        //    obfuscationExecutor.Dispose();
        //}

        [Test]
        public void ReadTables()
        {

            Faker _textGenerator = new Faker("de");
            for (int i = 0; i < 100; i++)
            {
                int _precision = 10;
                int _scale = 2;
                double decimalPart = _textGenerator.Random.Number(0, (int)Math.Pow(10, (double)_precision - _scale) - 1);

                double fractionaPart = _textGenerator.Random.Number(0, (int)Math.Pow(10, (double)_scale) - 1);

                fractionaPart = fractionaPart * Math.Pow(10, -_scale);
                var result = fractionaPart + decimalPart;

                if (_textGenerator.Random.Bool())
                {
                    result = -result;
                }
                
                //Console.WriteLine(_textGenerator.Finance.Amount(-200M, 20M, _scale));
                //Console.WriteLine(_textGenerator.Date.Between(DateTime.Now, DateTime.Now.AddYears(1)));
                Console.WriteLine(_textGenerator.Date.Between(new DateTime(2020,1,1,12,00,15), new DateTime(2020, 1, 1, 12, 00, 59)).TimeOfDay);

                ;
            }
            
        }

    }
}
