﻿//using System.Collections.Generic;
//using System.Linq;
//using NUnit.Framework;
//using Obfuscator.Domain;
//using Obfuscator.DomainServices.SettingsStorage;

//namespace Obfuscator.Test
//{
//    [TestFixture]
//    public class SettingsStoragTest
//    {
//        [Test]
//        public void SaveAndLoadTest()
//        {
//            Database db1 = new Database("con1");
//            Database db2 = new Database("con2");

//            Table t11 = new Table("t11");
//            Table t12 = new Table("t12");
//            Table t21 = new Table("t21");
//            Table t22 = new Table("t22");

//            ColumnObfuscation obfuscation = new ColumnObfuscation(
//                ObfuscatorType.Phone,
//                new List<ObfuscationParameter>
//                {
//                    new ObfuscationParameter("PhonePattern", "+48 ### ### ###")
//                }
//            );
            
//            Column c1_1 = new Column(db1, t11, obfuscation, "c11");
//            Column c1_2 = new Column(db1, t11, obfuscation, "c12");
//            Column c1_3 = new Column(db1, t12, obfuscation, "c13");
//            Column c1_4 = new Column(db1, t12, obfuscation, "c14");
//            Column c2_1 = new Column(db2, t21, obfuscation, "c21");
//            Column c2_2 = new Column(db2, t21, obfuscation, "c22");
//            Column c2_3 = new Column(db2, t22, obfuscation, "c23");
//            Column c2_4 = new Column(db2, t22, null, "c24");


//            SettingsStorage settingsStorage = new SettingsStorage();

//            ObfuscatorServiceSettigs settings = new ObfuscatorServiceSettigs();
            
//                settings.Columns.AddRange(new List<Column>()
//                {
//                    c1_1,
//                    c1_2,
//                    c1_3,
//                    c1_4,
//                    c2_1,
//                    c2_2,
//                    c2_3,
//                    c2_4
//                });
//                settings.Databases.AddRange(new List<Database>
//                {
//                    db1, db2
//                });
        

//            string fileName = "settings.xml";
//            settingsStorage.Save(settings, fileName);

//            var fromFile = settingsStorage.Load(fileName);

//            Assert.AreEqual(settings.Columns.Count,fromFile.Columns.Count);

//            foreach (Column fromFileColumn in fromFile.Columns.Where(c => c.Obfuscation != null))
//            {
//                Assert.AreEqual(1, fromFileColumn.Obfuscation.Parameters.Count);
//            }

//            Assert.AreEqual(1, fromFile.Columns.Count(c => c.Obfuscation == null));

//            Assert.AreEqual(4, fromFile.Columns.Count(c =>c.Database == db1));
//            Assert.AreEqual(4, fromFile.Columns.Count(c =>c.Database == db2));

//            Assert.AreEqual(2, fromFile.Columns.Count(c =>c.Table == t11));
//            Assert.AreEqual(2, fromFile.Columns.Count(c =>c.Table == t12));
//            Assert.AreEqual(2, fromFile.Columns.Count(c =>c.Table == t21));
//            Assert.AreEqual(2, fromFile.Columns.Count(c =>c.Table == t22));
//        }

//    }
//}
